// COMMANDE AR.DRONE
// VERSION 3 !

// Paul Guélorget

#ifndef _VOICE_H

// INDICES DU TABLEAU (ordre alphabétique)
#define SON_ARRET_URGENCE 0
#define SON_ATTERRISSAGE 1
#define SON_CONNEXION_PERDUE_APPAREIL 2
#define SON_CONNEXION_PERDUE_CONTROLEUR 3
#define SON_DECOLLAGE 4
#define SON_FIN_ARRET_URGENCE 5
#define SON_LOOPING 6
#define SON_MODE_NERVOSITE 7
#define SON_MODE_SECURITE 8
#define NOMBRE_DE_SONS 9

// TABLEAU DE POINTEURS SUR CHUNKS
Mix_Chunk *sons[NOMBRE_DE_SONS];

// FONCTIONS
void chargerTousWav(Mix_Chunk **array);  // charge les fichiers .wav dans les chunks pointés par les éléments du tableau sons.
void detruireTousWav(Mix_Chunk **array); // libération des chunks
void lireWav(int indiceTableau); // lis le son correspondant
#endif // _VOICE_H
