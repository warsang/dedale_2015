// COMMANDE AR.DRONE
// VERSION 3 !

// Paul Guélorget

#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include "voice.h"



void chargerTousWav(Mix_Chunk **array)
{
  Mix_AllocateChannels(NOMBRE_DE_SONS);
  Mix_Volume(-1, MIX_MAX_VOLUME);
  array[SON_ARRET_URGENCE] = Mix_LoadWAV("sons/arret_urgence.wav");
  array[SON_ATTERRISSAGE] = Mix_LoadWAV("sons/atterrissage.wav");
  array[SON_CONNEXION_PERDUE_APPAREIL] = Mix_LoadWAV("sons/connexion_perdue_appareil.wav");
  array[SON_CONNEXION_PERDUE_CONTROLEUR] = Mix_LoadWAV("sons/connexion_perdue_controleur.wav");
  array[SON_DECOLLAGE] = Mix_LoadWAV("sons/decollage.wav");
  array[SON_FIN_ARRET_URGENCE] = Mix_LoadWAV("sons/fin_arret_urgence.wav");
  array[SON_LOOPING] = Mix_LoadWAV("sons/looping.wav");  
  array[SON_MODE_NERVOSITE] = Mix_LoadWAV("sons/mode_nervosite.wav");
  array[SON_MODE_SECURITE] = Mix_LoadWAV("sons/mode_securite.wav");
}

void detruireTousWav(Mix_Chunk **array)
{
  int i;
  for(i=0;i<NOMBRE_DE_SONS;i++)
    Mix_FreeChunk(array[i]);
}

void lireWav(int indiceTableau)
{
  Mix_PlayChannel(-1, sons[indiceTableau], 0);
}

