#include <stdio.h>
#include <stdlib.h>
#include <opencv/highgui.h>
#include <opencv/cv.h>

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define abs(x) ((x) > 0 ? (x) : -(x))
#define sign(x) ((x) > 0 ? 1 : -1)

#define STEP_MIN 5
#define STEP_MAX 100

IplImage *image;
int r=0,g=0,b=0,tolerance=30;
CvPoint objectPos=cvPoint(-1,-1);

CvPoint binarisation(IplImage* image, int *nbPixels)
{
    int x,y;
    IplImage *mask;
    IplConvKernel *kernel;
    int sommeX=0,sommeY=0;
    *nbPixels=0;
    mask=cvCreateImage(cvGetSize(image),image->depth,1);
    cvInRangeS(image,cvScalar(r-tolerance,g-tolerance,b-tolerance),cvScalar(r+tolerance,g+tolerance,b+tolerance),mask);
    kernel=cvCreateStructuringElementEx(5,5,2,2,CV_SHAPE_ELLIPSE);
    cvDilate(mask,mask,kernel,1);
    cvErode(mask,mask,kernel,1);

    for(y=0;y<mask->height;y++)
    {
        for(x=0;x<mask->width;x++)
        {
            if(((uchar*)(mask->imageData + y*mask->width))[x] == 255)
            {
                sommeX += x;
                sommeY += y;
                (*nbPixels)++;
            }
        }
    }

    cvShowImage("Dedale - Traitement video",mask);
    cvReleaseStructuringElement(&kernel);
    cvReleaseImage(&mask);

    if((*nbPixels)>0) return cvPoint((int)(sommeX/(*nbPixels)),(int)(sommeY/(*nbPixels)));
    else return cvPoint(-1,-1);
}

void addObjectToVideo(IplImage* image, CvPoint objectNextPos, int nbPixels) {

    int objectNextStepX, objectNextStepY;

    if (nbPixels > 10) {

        if (objectPos.x == -1 || objectPos.y == -1) {
            objectPos.x = objectNextPos.x;
            objectPos.y = objectNextPos.y;
        }

        if (abs(objectPos.x - objectNextPos.x) > STEP_MIN) {
            objectNextStepX = max(STEP_MIN, min(STEP_MAX, abs(objectPos.x - objectNextPos.x) / 2));
            objectPos.x += (-1) * sign(objectPos.x - objectNextPos.x) * objectNextStepX;
        }
        if (abs(objectPos.y - objectNextPos.y) > STEP_MIN) {
            objectNextStepY = max(STEP_MIN, min(STEP_MAX, abs(objectPos.y - objectNextPos.y) / 2));
            objectPos.y += (-1) * sign(objectPos.y - objectNextPos.y) * objectNextStepY;
        }

    } else {

        objectPos.x = -1;
        objectPos.y = -1;

    }

    if (nbPixels > 10)
        cvDrawCircle(image, objectPos, 15, CV_RGB(255, 0, 0), -1);

    cvShowImage("Dedale - Video drone", image);

}

void getObjectColor(int event, int x, int y, int flags, void *param=NULL)
{
    CvScalar p;

    if(event==CV_EVENT_LBUTTONUP)
    {
        p=cvGet2D(image,y,x);
        r=(int)p.val[0];
        g=(int)p.val[1];
        b=(int)p.val[2];
    }
}

int main (int argc, char* argv[])
{
    CvCapture *capture=NULL;
    CvPoint objectNextPos;
    char key;
    int nbPixels;

    if(argc<2)
    {
        printf("usage : %s Flux_Source\n",argv[0]);
        return EXIT_FAILURE;
    }
    capture=cvCreateFileCapture(argv[1]);
    if(!capture)
    {
        fprintf(stderr,"Impossible de charger le flux video\n");
        return EXIT_FAILURE;
    }


    cvNamedWindow("Dedale - Video drone", CV_WINDOW_AUTOSIZE);
    cvNamedWindow("Dedale - Traitement video", CV_WINDOW_AUTOSIZE);
    cvMoveWindow("Dedale - Video drone",0,100);
    cvMoveWindow("Dedale - Traitement video",683,100);

    cvSetMouseCallback("Dedale - Video drone", getObjectColor);

        while(key != 'q' && key != 'Q')
        {
            image = cvQueryFrame(capture);
            if(!image) continue;
            objectNextPos = binarisation(image, &nbPixels);
            addObjectToVideo(image, objectNextPos, nbPixels);
            key = cvWaitKey(1);
        }
    cvWaitKey(0);
    cvReleaseCapture(&capture);
    cvDestroyAllWindows();
    return EXIT_SUCCESS;
}
