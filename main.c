#include "drone.h"

typedef struct Drone Drone;

int main(void)
{
    Drone *drone = createDrone();

    if(drone == NULL) // if drone is not created
        exit(1);

    while(updateDrone(drone) > 0);

    destroyDrone(drone);

    return 0;
}
