#ifndef GAMECONTROLLER_H_INCLUDED
#define GAMECONTROLLER_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>


typedef enum Button{BUTTON_Y, BUTTON_B, BUTTON_A, BUTTON_X, BUTTON_RSHOULDER, BUTTON_LSHOULDER, BUTTON_MAX} Button;
typedef enum Axis{AXIS_LEFTX, AXIS_LEFTY, AXIS_RIGHTX, AXIS_RIGHTY, AXIS_MAX} Axis;

#define AXIS_LEFTX 0
#define AXIS_LEFTY 1
#define AXIS_RIGHTX 2
#define AXIS_RIGHTY 3
#define BUTTON_Y 0
#define BUTTON_B 1
#define BUTTON_A 2
#define BUTTON_X 3
#define BUTTON_RSHOULDER 4
#define BUTTON_LSHOULDER 5

// For file configuration
struct JoystickConfig
{
    int axis[AXIS_MAX];
    char button[BUTTON_MAX];
};

struct GameController
{
    SDL_GameController *gameController;
    SDL_Joystick *joystick; // if not game controller
    struct JoystickConfig joystickConfig; // if not game controller too

    char button[BUTTON_MAX];
    int axis[AXIS_MAX];
};

struct GameController *createGameController(void);

int updateGameController(struct GameController *gc);

void destroyGameController(struct GameController *gc);

#endif // GAMECONTROLLER_H_INCLUDED
