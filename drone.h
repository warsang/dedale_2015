#ifndef DRONE_H_INCLUDED
#define DRONE_H_INCLUDED

#include "gamecontroller.h"
#include "socketudp.h"

struct Drone
{
    struct GameController *gameController;
    struct SocketUDP *socketUDP;
    unsigned counter;
    int take_off;
};

struct Drone *createDrone(void);

int updateDrone(struct Drone *drone);

void destroyDrone(struct Drone *drone);

#endif // DRONE_H_INCLUDED
