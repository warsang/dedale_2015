#ifndef SOCKETUDP_H_INCLUDED
#define SOCKETUDP_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#define ADRESS_IP "192.168.1.1"
#define PORT_AT 5556
#define BUFLEN 256 // Size of UDP batch
#define DELAY 45000



struct SocketUDP
{
    int fd;
    struct sockaddr_in serv_addr;
};

struct SocketUDP *createSocketUDP(void);
void sendAT(struct SocketUDP *sckt, char* cmd, unsigned int *c, char* arg);
void destroySocketUDP(struct SocketUDP *sckt);

#endif // SOCKETUDP_H_INCLUDED
