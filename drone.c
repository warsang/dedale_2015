#include "drone.h"

#define SAFE_EULER "\"control:euler_angle_max\",\"0.25\""
#define SAFE_ALTITUDE_MAX "\"control:altitude_max\",\"5000\""
#define SAFE_ALTITUDE_MIN "\"control:altitude_min\",\"50\""
#define SAFE_VERTICALE_SPEED "\"control:control_vz_max\",\"800\""
#define SAFE_YAW "\"control:control_yaw\",\"3.0\""

typedef enum {
	ANIM_PHI_M30_DEG,
	ANIM_PHI_30_DEG,
	ANIM_THETA_M30_DEG,
	ANIM_THETA_30_DEG,
	ANIM_THETA_20DEG_YAW_200DEG,
	ANIM_THETA_20DEG_YAW_M200DEG,
	ANIM_TURNAROUND,
	ANIM_TURNAROUND_GODOWN,
	ANIM_YAW_SHAKE,
	ANIM_YAW_DANCE,
	ANIM_PHI_DANCE,
	ANIM_THETA_DANCE,
    ANIM_VZ_DANCE,
	ANIM_WAVE,
	ANIM_PHI_THETA_MIXED,
	ANIM_DOUBLE_PHI_THETA_MIXED,
	ANIM_FLIP_AHEAD,
	ANIM_FLIP_BEHIND,
	ANIM_FLIP_LEFT,
	ANIM_FLIP_RIGHT,
}DroneAnim;

#define TAKE_OFF "290718208"
#define LANDING  "290717696"

#define THRESHOLD_CONTROLLER 8000

#define ANIM_DRONE_ARG "\"control:flight_anim\""

typedef struct Drone Drone;

void controlDrone(Drone *drone);

Drone *createDrone(void)
{
    Drone *drone = NULL;

    if(SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER | SDL_INIT_EVENTS) < 0)
    {
        printf("%s", SDL_GetError());
        return NULL;
    }

    drone = calloc(1, sizeof(Drone));

    if(drone == NULL)
        return NULL;

    if((drone->gameController = createGameController()) == NULL)
    {
        printf("No controller find");
        free(drone);
        return NULL;
    }

    if((drone->socketUDP = createSocketUDP()) == NULL)
    {
        destroyGameController(drone->gameController);
        free(drone);
        return NULL;
    }

    drone->counter = 0;

    drone->take_off = 0;

    // Configuration du drone
    sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, SAFE_EULER);
    sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, SAFE_ALTITUDE_MIN);
    sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, SAFE_ALTITUDE_MAX);
    sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, SAFE_YAW);
    sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, SAFE_VERTICALE_SPEED);

    printf("Press R1/RB to take_off, L1/LB to land\n");

    return drone;
}

int updateDrone(Drone *drone)
{
    static char arg[BUFLEN];
    int state = updateGameController(drone->gameController);

    char const *button = drone->gameController->button;

    if(button[BUTTON_RSHOULDER] && drone->take_off == 0)
    {
        printf("Décollage du drône\n");
        drone->take_off = 1;
        sendAT(drone->socketUDP, "AT*REF", &drone->counter, TAKE_OFF);
    }

    if(button[BUTTON_LSHOULDER] && drone->take_off == 1)
    {
        printf("Aterrissage du drône\n");
        drone->take_off = 0;
        sendAT(drone->socketUDP, "AT*REF", &drone->counter, LANDING);
    }

    if(button[BUTTON_Y])
    {
        sprintf(arg, "%s,\"%d,%d\"", ANIM_DRONE_ARG, ANIM_FLIP_AHEAD, 20);
        sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, arg);
    }

    if(button[BUTTON_X])
    {
        sprintf(arg, "%s,\"%d,%d\"", ANIM_DRONE_ARG, ANIM_FLIP_LEFT, 20);
        sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, arg);
    }

    if(button[BUTTON_B])
    {
        sprintf(arg, "%s,\"%d,%d\"", ANIM_DRONE_ARG, ANIM_FLIP_RIGHT, 20);
        sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, arg);
    }

    if(button[BUTTON_A])
    {
        sprintf(arg, "%s,\"%d,%d\"", ANIM_DRONE_ARG, ANIM_FLIP_BEHIND, 20);
        sendAT(drone->socketUDP, "AT*CONFIG", &drone->counter, arg);
    }

    controlDrone(drone);

    return state;
}

//left-right, front-back, angle speed, vert-speed
void controlDrone(Drone *drone)
{
    static char arg[BUFLEN];
    float roll, pitch, yaw, gaz;
    int iRoll, iPitch, iYaw, iGaz;

    int const *axis = drone->gameController->axis;

    roll = (float)  axis[AXIS_LEFTX];
    pitch = (float) axis[AXIS_LEFTY] * -1;
    yaw = (float)   axis[AXIS_RIGHTX];
    gaz = (float)   axis[AXIS_RIGHTY] * -1;

    if(abs(roll) < THRESHOLD_CONTROLLER)
        roll = 0.0;

    if(abs(pitch) < THRESHOLD_CONTROLLER)
        pitch = 0.0;

    if(abs(yaw) < THRESHOLD_CONTROLLER)
        yaw = 0.0;

    if(abs(gaz) < THRESHOLD_CONTROLLER)
        gaz = 0.0;

    roll  /= 32767.0;
    pitch /= 32767.0;
    yaw   /= 32767.0;
    gaz   /= 32767.0;

    iRoll  = *(int *)&roll;
    iPitch = *(int *)&pitch;
    iYaw   = *(int *)&yaw;
    iGaz   = *(int *)&gaz;

    sprintf(arg, "1,%d,%d,%d,%d", iRoll, iPitch, iGaz, iYaw);

    sendAT(drone->socketUDP, "AT*PCMD", &drone->counter, arg);
}

void destroyDrone(Drone *drone)
{
    destroyGameController(drone->gameController);
    destroySocketUDP(drone->socketUDP);
    free(drone);
    SDL_Quit();
}
