#include "gamecontroller.h"

typedef struct GameController GameController;

typedef struct JoystickConfig JoystickConfig;

int getButtonConfigureJoystick(SDL_Joystick *joystick)
{
    int i = 0;

    while(1)
    {
        SDL_JoystickUpdate();

        if(SDL_JoystickGetButton(joystick, i % SDL_JoystickNumButtons(joystick)) > 0)
            return i % SDL_JoystickNumButtons(joystick);

        ++i;
    }
}

int getAxisConfigureJoystick(SDL_Joystick *joystick)
{
    int i = 0;

    while(1)
    {
        SDL_JoystickUpdate();

        if(SDL_JoystickGetAxis(joystick, i % SDL_JoystickNumAxes(joystick)) > 30000)
            return i % SDL_JoystickNumAxes(joystick);

        ++i;
    }
}

void configureJoystick(SDL_Joystick *joystick, JoystickConfig *config)
{
    int i = 0;
    int j = 0;

    static char const *buttonName[] = {"Y or Triangle", "B or Circle", "A or Cross",
                                       "X or Square", "R1 or RB", "L1 or LB"};

    static char const *axisName[] = {"Left Axis Horizontal : In Right Direction",  "Left Axis Vertical : In Down Direction",
                                     "Right Axis Horizontal : In Right Direction", "Right Axis Vertical : In Down Direction"};

    for(i = 0; i != BUTTON_MAX; ++i)
    {
        printf("Press %s\n", buttonName[i]);

        // Because I'm a genius
        again_button:
        config->button[i] = getButtonConfigureJoystick(joystick);

        for(j = 0; j != i; ++j)
            if(config->button[i] == config->button[j])
                goto again_button;
    }

    for(i = 0; i != AXIS_MAX; ++i)
    {
        printf("Push %s\n", axisName[i]);

        again_axis:
        config->axis[i] = getAxisConfigureJoystick(joystick);

        for(j = 0; j != i; ++j)
            if(config->axis[i] == config->axis[j])
                goto again_axis;
    }

    printf("Your controller is configure\n\n");
}

GameController *createGameController(void)
{
    if(SDL_NumJoysticks() == 0)
        return NULL;

    GameController *gc = calloc(1, sizeof(GameController));

    if(gc == NULL)
        return NULL;

    if(SDL_IsGameController(0))
    {
        gc->gameController = SDL_GameControllerOpen(0);
        SDL_GameControllerEventState(SDL_IGNORE);

        if(gc->gameController == NULL)
        {
            free(gc);
            return NULL;
        }
    }

    else
    {
        static char path[4096];
        FILE *fileConfig;

        gc->joystick = SDL_JoystickOpen(0);
        SDL_JoystickEventState(SDL_IGNORE);

        if(gc->joystick == NULL)
        {
            free(gc);
            return NULL;
        }

        sprintf(path, "Joystick/%s.dedale", SDL_JoystickName(gc->joystick));

        fileConfig = fopen(path, "rb");

        // File exit : Joystick known
        if(fileConfig != NULL)
            fread(&gc->joystickConfig, sizeof gc->joystickConfig, 1, fileConfig);


        // Otherwise, we need to create one other
        else
        {
            fileConfig = fopen(path, "wb");
            printf("We are going to configure your controller\n");

            configureJoystick(gc->joystick, &gc->joystickConfig);

            fwrite(&gc->joystickConfig, sizeof gc->joystickConfig, 1, fileConfig);
        }

        fclose(fileConfig);
    }

    return gc;
}

int updateGameController(GameController *gc)
{
    static int state = 1;

    if(gc->gameController != NULL)
    {
        SDL_GameControllerUpdate();
        gc->button[BUTTON_A] = SDL_GameControllerGetButton(gc->gameController, SDL_CONTROLLER_BUTTON_A);
        gc->button[BUTTON_B] = SDL_GameControllerGetButton(gc->gameController, SDL_CONTROLLER_BUTTON_B);
        gc->button[BUTTON_X] = SDL_GameControllerGetButton(gc->gameController, SDL_CONTROLLER_BUTTON_X);
        gc->button[BUTTON_Y] = SDL_GameControllerGetButton(gc->gameController, SDL_CONTROLLER_BUTTON_Y);
        gc->button[BUTTON_LSHOULDER] = SDL_GameControllerGetButton(gc->gameController, SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
        gc->button[BUTTON_RSHOULDER] = SDL_GameControllerGetButton(gc->gameController, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);

        gc->axis[AXIS_LEFTX] = SDL_GameControllerGetAxis(gc->gameController, SDL_CONTROLLER_AXIS_LEFTX);
        gc->axis[AXIS_LEFTY] = SDL_GameControllerGetAxis(gc->gameController, SDL_CONTROLLER_AXIS_LEFTY);
        gc->axis[AXIS_RIGHTX] = SDL_GameControllerGetAxis(gc->gameController, SDL_CONTROLLER_AXIS_RIGHTX);
        gc->axis[AXIS_RIGHTY] = SDL_GameControllerGetAxis(gc->gameController, SDL_CONTROLLER_AXIS_RIGHTY);
    }

    else
    {
        SDL_JoystickUpdate();
        gc->button[BUTTON_A] = SDL_JoystickGetButton(gc->joystick, gc->joystickConfig.button[BUTTON_A]);
        gc->button[BUTTON_B] = SDL_JoystickGetButton(gc->joystick, gc->joystickConfig.button[BUTTON_B]);
        gc->button[BUTTON_X] = SDL_JoystickGetButton(gc->joystick, gc->joystickConfig.button[BUTTON_X]);
        gc->button[BUTTON_Y] = SDL_JoystickGetButton(gc->joystick, gc->joystickConfig.button[BUTTON_Y]);
        gc->button[BUTTON_LSHOULDER] = SDL_JoystickGetButton(gc->joystick, gc->joystickConfig.button[BUTTON_LSHOULDER]);
        gc->button[BUTTON_RSHOULDER] = SDL_JoystickGetButton(gc->joystick, gc->joystickConfig.button[BUTTON_RSHOULDER]);

        gc->axis[AXIS_LEFTX] = SDL_JoystickGetAxis(gc->joystick, gc->joystickConfig.axis[AXIS_LEFTX]);
        gc->axis[AXIS_LEFTY] = SDL_JoystickGetAxis(gc->joystick, gc->joystickConfig.axis[AXIS_LEFTY]);
        gc->axis[AXIS_RIGHTX] = SDL_JoystickGetAxis(gc->joystick, gc->joystickConfig.axis[AXIS_RIGHTX]);
        gc->axis[AXIS_RIGHTY] = SDL_JoystickGetAxis(gc->joystick, gc->joystickConfig.axis[AXIS_RIGHTY]);
    }

    return state;
}

void destroyGameController(GameController *gc)
{
    if(gc->gameController != NULL)
        SDL_GameControllerClose(gc->gameController);

    else
        SDL_JoystickClose(gc->joystick);

    free(gc);
}
