#include "socketudp.h"

typedef struct SocketUDP SocketUDP;

SocketUDP *createSocketUDP(void)
{
    SocketUDP *sckt = calloc(1, sizeof(SocketUDP));

    if(sckt == NULL)
        return NULL;

    sckt->fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if(sckt->fd < 0)
    {
        perror("Socket : ");
        free(sckt);
        return NULL;
    }

    sckt->serv_addr.sin_family = AF_INET;
    sckt->serv_addr.sin_port = htons(PORT_AT);

    if(inet_aton(ADRESS_IP, &sckt->serv_addr.sin_addr) == 0)
    {
        printf("inet_aton error\n");
        close(sckt->fd);
        free(sckt);
        return NULL;
    }

    return sckt;
}

void sendAT(SocketUDP *sckt, char* cmd, unsigned int *c, char* arg)
{
    static char message[BUFLEN];

    ++(*c);

    sprintf(message, "%s=%d,%s\r", cmd, *c, arg);

    if (sendto(sckt->fd, message, BUFLEN, 0,
              (struct sockaddr*)&sckt->serv_addr,
               sizeof(struct sockaddr)) == -1)
    {
        perror("sendto() ");
        return;
    }

    usleep(DELAY);
}

void destroySocketUDP(SocketUDP *sckt)
{
    close(sckt->fd);
    free(sckt);
}
